<!DOCTYPE html>
<html>

<head>
    <title></title>
</head>

<body>
    <h2>Buat Account Baru!</h2>
    <h4>Sign Up Form</h4>
    <form class="form-horizontal" action="/welcome" method="post">
        <fieldset>
            <!-- Text input-->
            <div class="form-group">
                @csrf
                <label class="col-md-4 control-label" for="First name :">First name :</label>
                <div class="col-md-4"><br>
                    <input id="First name :" name="First name :" type="text" placeholder="" class="form-control input-md">

                </div>
            </div><br>

            <!-- Text input-->
            <div class="form-group">
                @csrf
                <label class="col-md-4 control-label" for="textinput">Last name :</label>
                <div class="col-md-4"><br>
                    <input id="textinput" name="Last name" type="text" placeholder="" class="form-control input-md">

                </div>
            </div><br>

            <!-- Multiple Radios -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="radios">Gender :</label>
                <div class="col-md-4"><br>
                    <div class="radio">
                        <label for="radios-0">
                            <input type="radio" name="radios" id="radios-0" value="1" checked="checked">
                            Male
                        </label>
                    </div>
                    <div class="radio">
                        <label for="radios-1">
                            <input type="radio" name="radios" id="radios-1" value="2">
                            Female
                        </label>
                    </div>
                    <div class="radio">
                        <label for="radios-2">
                            <input type="radio" name="radios" id="radios-2" value="">
                            Other
                        </label>
                    </div>
                </div>
            </div><br>
            <div class="form-group">
                <label class="col-md-4 control-label" for="selectbasic">Nasionality :</label>
                <div class="col-md-4"><br>
                    <select id="selectbasic" name="selectbasic" class="form-control">
                        <option value="1">Indonesian</option>
                        <option value="2">Malaysian</option>
                    </select>
                </div>
            </div><br>

            <!-- Multiple Checkboxes -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="checkboxes">Language Spoken :</label>
                <div class="col-md-4"><br>
                    <div class="checkbox">
                        <label for="checkboxes-0">
                            <input type="checkbox" name="checkboxes" id="checkboxes-0" value="1">
                            Bahasa Indonesia
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="checkboxes-1">
                            <input type="checkbox" name="checkboxes" id="checkboxes-1" value="2">
                            English
                        </label>
                    </div>
                    <div class="checkbox">
                        <label for="checkboxes-2">
                            <input type="checkbox" name="checkboxes" id="checkboxes-2" value="">
                            Other
                        </label>
                    </div>
                </div>
            </div><br>
            <!-- Textarea -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="textarea">Bio :</label>
                <div class="col-md-4"><br>
                    <textarea class="form-control" id="textarea" name="textarea"></textarea>
                </div>
            </div>
            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="singlebutton"></label>
                <div class="col-md-4">
                    <button id="singlebutton" name="singlebutton" class="btn btn-inverse"> <a href="/welcome">Sign UP</a> </button>
                </div>
            </div>
        </fieldset>
    </form>

</body>

</html>