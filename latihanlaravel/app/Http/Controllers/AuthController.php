<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function second()
    {
        return view('halaman.form');
    }

    public function third(Request $request)
    {
        $nama = $request['First name'];
        $lengkap = $request['Last name'];
        return view('halaman.welcome', compact('nama', 'lengkap'));
    }
}
